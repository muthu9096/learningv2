import React,{  useState} from 'react';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import Projects from './components/Projects/Projects';
import About from './components/About/About';
import ChangeThemeButton from './components/ChangeThemeButton/ChangeThemeButton';
import { ThemeProvider } from './components/ThemeContext';
import Contact from './components/Contact/Contact';
// import { HashRouter, Route, Routes } from 'react-router-dom';
import style from './App.module.css';



function App() {
  const [ selectedTab, renderPage ]  = useState('home');
  const [theme, themeChange] = useState('light')
  const tabsList = [
       { id: 'home', text: 'Home' },
       { id: 'project', text: 'Projects' },
       { id: 'about', text: 'About' },
       { id: 'contact', text: 'Contact' }
  ];
  function handleClick(tab) {
    if (selectedTab !== tab) {
      renderPage(tab);
    }
  }
  function onchangeTheme(){
    themeChange((theme)=>{
       return theme === 'light' ? 'dark' : 'light';
    })
  }
  
  return (
    <ThemeProvider value={{theme: theme,changeTheme:onchangeTheme}}>
        <div className={style.container}>
          <ChangeThemeButton/>
          <Header
            tabsList={tabsList}
            onSelectTab={handleClick}
            selectedId={selectedTab}
          />
          <div className={style.contentContainer}>
            {selectedTab === 'home' && <Home />}
            {selectedTab === 'project' && <Projects />}
            {selectedTab === 'about' && <About />}
            {selectedTab === 'contact' && <Contact />}
          </div>
        </div>
    </ThemeProvider>
  )
}
export default App;


// export default class App extends React.Component {
//   constructor(props){
//     super(props);
//     this.handleClick = this.handleClick.bind(this);
//     this.onchangeTheme = this.onchangeTheme.bind(this);
//     this.state = {selectedTab: 'home',theme: 'light'}
//   }
//   handleClick(tab){
//     this.setState({
//       selectedTab: tab
//     })
//   }
//   onchangeTheme(){
//     this.setState(prevState => {
//       return { theme: prevState.theme === 'light' ? 'dark' : 'light'}
//     })
//   }
//   render() {
//     const tabsList = [
//              { id: 'home', text: 'Home' },
//              { id: 'project', text: 'Projects' },
//              { id: 'about', text: 'About' },
//              { id: 'contact', text: 'Contact' }
//         ];
//     let {theme,selectedTab} = this.state;
//     console.log(theme);
//     return (
//         <ThemeProvider value={{theme: theme, changeTheme: this.onchangeTheme}}>
//         <div className={style.container}>
//           <ChangeThemeButton/>
//             <Header
//               tabsList={tabsList}
//               onSelectTab={this.handleClick}
//               selectedId={selectedTab}
//             />
//           <div className={style.contentContainer}>
//             <div className={style.contentContainer}>
//               {selectedTab === 'home' && <Home />}
//               {selectedTab === 'project' && <Projects />}
//               {selectedTab === 'about' && <About />}
//               {selectedTab === 'contact' && <Contact />}
//             </div>
//           </div>
//         </div>
//         </ThemeProvider>

//     );
//   }
// }


// export default class App extends React.Component {
//   render() {
//     return (
//       <HashRouter>
//         <div className={style.container}>
//           <ChangeThemeButton/>
//           <Header />
//           <div className={style.contentContainer}>
//             <Routes>
//               <Route path='/home' element={<Home />}></Route>
//               <Route path='/projects' element={<Projects />}></Route>
//               <Route path='/about' element={<About />}></Route>
//               <Route path='/contact' element={<Contact />}></Route>
//               <Route path='/' element={<Home />}></Route>
//             </Routes>
//           </div>
//         </div>
//       </HashRouter>
//     );
//   }
// }