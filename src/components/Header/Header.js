import React from 'react';
import PropTypes from 'prop-types';
import Tab from '../Tab/Tab';
import { ThemeConsumer } from '../ThemeContext';
import style from './Header.module.css';
export default class Header extends React.Component {
  render() {
    let { tabsList, onSelectTab, selectedId } = this.props;
   return (
    <ThemeConsumer>
      {({theme})=>{
        return (
          <div className={`${style.container} ${style[theme]}`}>
                    <div className={`${style.name} ${style[theme]}`}>DM.</div>  
                <div className={style.tabsWrap}>
                  <Tab tabsList={tabsList} onSelect={onSelectTab} selectedId={selectedId}/>
                </div>
          </div>
        )
      }}
    </ThemeConsumer>
   );
  }
}

Header.propTypes = {
  tabsList: PropTypes.object,
  selectedId: PropTypes.string,
  onSelectTab: PropTypes.func
}