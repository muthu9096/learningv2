import React from 'react';
import PropTypes from 'prop-types';
import { ThemeConsumer } from '../ThemeContext';
import style from './Button.module.css';
export default class Button extends React.Component {
  render() {
      let { text,isUpperCase } = this.props;
   return (
       <ThemeConsumer>
         {({theme})=>{
            return (
                <div className={`${style.button} ${isUpperCase ? style.upperCase : ''} ${style[theme]}`}>{text}</div>
            )
         }}
       </ThemeConsumer>
   );
  }
}
Button.defaultProps = {
    isUpperCase: false
}
Button.propTypes = {
    text: PropTypes.string,
    isUpperCase: PropTypes.bool
}