import React, { useState, useEffect, useContext } from 'react';
import PageHeading from '../PageHeading/PageHeading';
import { ThemeContext } from '../ThemeContext';
import style from './Contact.module.css';

function Contact() {
  const [isMounted, showComponentMount] = useState(false);
  const {theme} = useContext(ThemeContext);

  useEffect(() => {
    showComponentMount(true);
  }, []);

  return (
    <div className={`${style.contactContainer} ${style[theme]}`}>
      {isMounted && <div className={style.mounted}>Mounted</div>}
      <div className={style.headerWrap}>
        <PageHeading name='Get in touch' tabName='Contact' palette='white' />
      </div>
      <div className={style.contactDetail}>
        <div className={style.contactWrap}>
          <input className={`${style.input} ${style[theme]}`} placeholder='Name' />
          <input className={`${style.input} ${style[theme]}`} placeholder='Email' />
          <textarea className={`${style.textarea} ${style[theme]}`} placeholder='Message' />
        </div>
      </div>
    </div>
  );

}
export default Contact;