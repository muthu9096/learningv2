import React from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from '../ThemeContext';
import style from './PageHeading.module.css';
export default class PageHeading extends React.Component {
  render() {
      let { name, tabName,palette } = this.props;
   return (
        <ThemeContext>
            {({theme})=>{
                return (
                    <div className={`${style.pageHeadingWrap} ${style[palette]} ${style[theme]}`}>
                        <div className={style.name}>{name}</div>
                        <div className={style.tabName}>{tabName}</div>   
                    </div>
                );
            }}
           
        </ThemeContext>
   );
  }
}
PageHeading.defaultProps = {
    palette: 'black'
};
PageHeading.propTypes = {
    name: PropTypes.string,
    tabName: PropTypes.string,
    palette: PropTypes.oneOfType(['white','black'])
}