import React,{useState,useEffect, useContext} from 'react';
import PageHeading from '../PageHeading/PageHeading';
import Profile from '../../images/Profile.png';
import { ThemeContext } from '../ThemeContext';
import style from './About.module.css';
import Button from '../Button/Button';

function About() {
    const [isMounted, showComponentMount] = useState(false);
    const {theme} = useContext(ThemeContext);
    const skillList = ['html', 'css', 'Java script', 'Bootstrap', 'Php', 'Phython', 'C++']

    useEffect(() => {
        showComponentMount(true);
    }, [])

    return (
        <div className={`${style.aboutContainer} ${style[theme]}`}>
            {isMounted && <div className={style.mounted}>Mounted</div>}
            <div className={style.headerWrap}><PageHeading name='Who I am' tabName='About' /></div>
            <div className={style.aboutBox}>
                <div className={style.profileBox}>
                    <img alt='img' src={Profile} />
                </div>
                <div>
                    <div className={`${style.about} ${style[theme]}`}>I implement user interface design and solve user problems with HTML, CSS and Javascript. I have 2 years of making products that solve user problems and Implementing responsive websites.</div>
                    <div className={style.skillBox}>
                        <div className={`${style.skillHeading} ${style[theme]}`}>Skills</div>
                    </div>
                    {skillList.map((skillName) => {
                        return <span className={style.tagWrap}><Button text={skillName} isUpperCase /></span>
                    })}
                </div>
            </div>
        </div>
    );

}
export default About;



// export default class About extends React.Component {
//     render() {
//       const skillList = ['html','css','Java script','Bootstrap','Php','Phython','C++']
//    return (
//        <div className={style.aboutContainer}>
//            {isMounted && <div className={`${style.mounted} ${style.mountedBox}`}>Mounted</div>}
//            <div className={style.headerWrap}><PageHeading name='Who I am' tabName='About' /></div>
//            <div className={style.aboutBox}>
//                <div className={style.profileBox}>
//                    <img src={Profile} />
//                </div>
//                <div>
//                     <div className={style.about}>I implement user interface design and solve user problems with HTML, CSS and Javascript. I have 2 years of making products that solve user problems and Implementing responsive websites.</div>
//                     <div className={style.skillBox}>
//                         <div className={style.skillHeading}>Skills</div>
//                     </div>
//                    {skillList.map((skillName) => {
//                        return <span className={style.tagWrap}><Button text={skillName} isUpperCase /></span>
//                    })}
//                </div>
//            </div>
//       </div>
//    );
//   }
// }