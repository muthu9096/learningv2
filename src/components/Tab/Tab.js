import React from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from '../ThemeContext';
// import { Link,useLocation } from 'react-router-dom';
import style from './Tab.module.css';

export default class Tab extends React.Component {
   render() {
      let { tabsList, selectedId,onSelect} = this.props;
     return (
      <ThemeContext>
         {({theme})=>{
            return (
               <div className={style.tabConatiner}>
                  {tabsList.map((list, index) => {
                     return <div
                        key={index}
                        id={list.id}
                        className={`${style.tab} ${selectedId === list.id ? style.active : ''} ${style[theme]}`}
                        onClick={()=>onSelect(list.id)}
                     >
                        {list.text}
                     </div>
                  })}
               </div>
            )
         }}
       </ThemeContext>
    );
   }
}
Tab.propTypes = {
   tabsList: PropTypes.object,
   selectedId: PropTypes.string,
   onSelect: PropTypes.func
}

// export default function Tab() {
//    let pathName = useLocation().pathname.split('/');
//    let tabsList = [
//       { id: '1', text: 'Home' },
//       { id: '2', text: 'Projects' },
//       { id: '3', text: 'About' },
//       { id: '4', text: 'Contact' }
//    ];
//    return (
//       <div className={style.tabConatiner}>
//          {tabsList.map((list, index) => {
//             let { text } = list;
//             return <Link
//                key={index}
//                className={`${style.tab}
//               ${(pathName[1] === list.text) || (pathName[1] ==='' && list.text =='Home') ? style.active : ''}`}
//                to={`/${text}`}
//             >
//                {text}
//             </Link>
//          })}
//       </div>
//    );
// }