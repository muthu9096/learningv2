import React from 'react';
import Profile from '../../images/Profile.png';
import Button from '../Button/Button';
import { ThemeConsumer } from '../ThemeContext';
import style from './Home.module.css';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isMounted: false}
    } 
    componentDidMount() {
        this.setState({isMounted: true})
    }
    render() {
        let { isMounted } = this.state;
   return (
      <ThemeConsumer>
       {({theme})=>{
         return (
            <div className={`${style.homeContainer} ${style[theme]}`}>
                <div className={style.profileBox}>
                    <img alt='img' src={Profile}/>
                </div>
                <div>
                    {isMounted && <div className={style.mounted}>Mounted</div>}
                    <div className={`${style.name} ${style[theme]}`}>MUTHUMARI</div>
                    <div className={`${style.role} ${style[theme]}`}>FRONTEND DEVELOPER</div>
                    <div className={`${style.about} ${style[theme]}`}>I implement user interface design and solve user problems with HTML, CSS and Javascript. I have 2 years of making products that solve user problems and Implementing responsive websites.</div>
                    <Button text='My Resume'/>
                </div>
            </div>
         )
       }}
      </ThemeConsumer>
   );
  }
}