import React, { useContext, useEffect, useState } from 'react';
import PageHeading from '../PageHeading/PageHeading';
import { ThemeContext } from '../ThemeContext';
import style from './Projects.module.css';


function Projects() {
  const [isMounted, showComponentMount] = useState(false);
  const {theme} = useContext(ThemeContext);

  useEffect(() => {
    showComponentMount(true);
  },[])
 
  return (
    <div className={`${style.container} ${style[theme]}`}>
      {isMounted && <div className={style.mounted}>Mounted</div>}
      <div className={style.headerWrap}>
        <PageHeading name='Projects' tabName='React' palette='white' />
      </div>
      <div className={style.projectsWrap}>
        <div className={`${style.project} ${style[theme]}`}></div>
        <div className={`${style.project} ${style[theme]}`}></div>
        <div className={`${style.project} ${style[theme]}`}></div>
      </div>
    </div>
  );
  
}
export default Projects;