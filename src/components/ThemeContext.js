import React from "react";

const ThemeContext = React.createContext({
    theme: 'light', changeTheme: () => {}
}
);
const ThemeProvider = ThemeContext.Provider;
const ThemeConsumer = ThemeContext.Consumer;

export {ThemeContext,ThemeProvider,ThemeConsumer};
